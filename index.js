module.exports = function({ addUtilities, config }) {
  const properties = config('transitionProperties', [
    'background-color',
    'transform'
  ])

  const speeds = config('transitionSpeeds', {
    slow: '200ms',
    normal: '150ms',
    fast: '100ms'
  })

  const origins = config('transitionOrigins', {
    t: 'top',
    r: 'right',
    b: 'bottom',
    l: 'left',
    tl: 'top left',
    tr: 'top right',
    bl: 'bottom left',
    br: 'bottom right'
  })

  const timingFunctions = config('transitionTimingFunctions', {
    cubic: 'cubic-bezier(0.4, 0, 0.2, 1)'
  })

  const prefix = '.transition'

  const utils = {}

  properties.forEach(prop => {
    utils[`${prefix}-${prop}`] = {
      'transition-property': prop
    }
  })

  for (let name in speeds) {
    utils[`${prefix}-speed-${name}`] = {
      'transition-duration': speeds[name]
    }
  }

  for (let origin in origins) {
    utils[`${prefix}-origin-${origin}`] = {
      'transform-origin': origins[origin]
    }
  }

  for (let fn in timingFunctions) {
    utils[`${prefix}-timing-function-${fn}`] = {
      'transition-timing-function': timingFunctions[fn]
    }
  }

  addUtilities(utils)
}
