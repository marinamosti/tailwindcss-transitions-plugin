# Deprecated

This plugin is no longer maintained. Tailwind now has it's own very good [set of transition classes](https://tailwindcss.com/docs/transition-property) that you should use instead :)

# Tailwind CSS Transitions Plugin

This is a very simple transitions plugin for Tailwind CSS which will add some utility classes.

## Installation

`npm install tailwindcss-transitions-plugin`

or 

`yarn add tailwindcss-transitions-plugin`

Add the following code to your `tailwind.config.js` file

```javascript
plugins: [
  require('tailwindcss-transitions-plugin')
]
```

## Usage

The plugin will generate a set of classes for you by default. The current classes will have the following format:

Class name: `.transition-{value}`
CSS Prop: `transition-property`

Class name: `.transition-speed-{value}`
CSS Prop: `transition-duration`

Class name: `.transition-origin-{value}`
CSS Prop: `tranform-origin-{value}`

Class name: `.transition-timing-function-{value}`
CSS Prop: `transition-timing-function`

In order to override the defaults, set the following properties inside your main `tailwind.config.js` object that is being exported. Feel free to completely ovewrride and/or add new values, they will be processed by the plugin.

```javascript
// Generated Ex: transition-background-color, transition-box-shadow
transitionProperties: ['background-color', 'transform', 'box-shadow'],

// Generated Ex: transition-speed-slow
transitionSpeeds: {
slow: '200ms',
normal: '150ms',
fast: '100ms'
},

// Generated Ex: transition-origin-tl
transitionOrigins: {
t: 'top',
r: 'right',
b: 'bottom',
l: 'left',
tl: 'top left',
tr: 'top right',
bl: 'bottom left',
br: 'bottom right'
},

// Generated ex: transition-function-cubic
transitionTimingFunctions: {
cubic: 'cubic-bezier(0.4, 0, 0.2, 1)'
},
```

Now apply to the element you're animating.

```html
<button class="transition-background-color transition-speed-slow transition-function-cubic">Has Transitions</button>
```

That's it :)

## Contributions

PRs welcome